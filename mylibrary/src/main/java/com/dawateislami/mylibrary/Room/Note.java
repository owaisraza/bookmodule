package com.dawateislami.mylibrary.Room;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Note {


    @SerializedName("books")
    @Expose
    private List<RoomBook> roomBooks = null;

    public List<RoomBook> getRoomBooks() {
        return roomBooks;
    }

    public void setRoomBooks(List<RoomBook> roomBooks) {
        this.roomBooks = roomBooks;
    }

}

