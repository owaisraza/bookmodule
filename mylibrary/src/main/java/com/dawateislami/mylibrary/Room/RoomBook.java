package com.dawateislami.mylibrary.Room;

import android.util.Log;

import com.dawateislami.mylibrary.View.BookLoadClass;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "book_table")
public class RoomBook {

    @ColumnInfo(defaultValue = "false")
    @Expose
    private boolean is_update;
    @Expose
    private Integer p_id;

    @SerializedName("id")
    @PrimaryKey
    @Expose
    private Integer id;
    @SerializedName("bookId")
    @Expose
    private Integer bookId;
    @SerializedName("bookJildNo")
    @Expose
    private Integer bookJildNo;
    @SerializedName("createdDate")
    @Expose
    private Long createdDate;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("appId")
    @Expose
    private Integer appId;
    @SerializedName("isEnabled")
    @Expose
    private Integer isEnabled;
    @SerializedName("isRecorded")
    @Expose
    private Integer isRecorded;
    @SerializedName("isUpcoming")
    @Expose
    private Integer isUpcoming;
    @SerializedName("isFeatured")
    @Expose
    private Integer isFeatured;
    @SerializedName("isbnNumber")
    @Expose
    private String isbnNumber;
    @SerializedName("modifiedDate")
    @Expose
    private Long modifiedDate;
    @SerializedName("nativeName")
    @Expose
    private String nativeName;
    @SerializedName("normalName")
    @Expose
    private String normalName;
    @SerializedName("publishedDate")
    @Expose
    private Long publishedDate;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("readCount")
    @Expose
    private Integer readCount;
    @SerializedName("readingType")
    @Expose
    private String readingType;
    @SerializedName("recommendedBook")
    @Expose
    private Integer recommendedBook;
    @SerializedName("romanName")
    @Expose
    private String romanName;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("realPages")
    @Expose
    private Integer realPages;
    @SerializedName("jumadaAlAwwal")
    @Expose
    private Integer jumadaAlAwwal;
    @SerializedName("jumadaAlThani")
    @Expose
    private Integer jumadaAlThani;
    @SerializedName("muharram")
    @Expose
    private Integer muharram;
    @SerializedName("rabiAlAwwal")
    @Expose
    private Integer rabiAlAwwal;
    @SerializedName("rabiAlThani")
    @Expose
    private Integer rabiAlThani;
    @SerializedName("rajab")
    @Expose
    private Integer rajab;
    @SerializedName("ramadan")
    @Expose
    private Integer ramadan;
    @SerializedName("safar")
    @Expose
    private Integer safar;
    @SerializedName("shaban")
    @Expose
    private Integer shaban;
    @SerializedName("shawwal")
    @Expose
    private Integer shawwal;
    @SerializedName("zul_hijjah")
    @Expose
    private Integer zulHijjah;
    @SerializedName("zul_qadah")
    @Expose
    private Integer zulQadah;
    @SerializedName("readBook")
    @Expose
    private Long readBook;
    @SerializedName("imageId")
    @Expose
    private Integer imageId;
    @SerializedName("audioId")
    @Expose
    private Integer audioId;
    @SerializedName("videoId")
    @Expose
    private Integer videoId;
    //@SerializedName("catagories")
    // @Expose
//        private List<Integer> catagories = null;
//        @SerializedName("mediaResponse")
//        @Expose
//        private Object mediaResponse;
//        @SerializedName("audioResponse")
//        @Expose
//        private Object audioResponse;
//        @SerializedName("imageResponse")
//        @Expose
//        private Object imageResponse;
    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("publisherName")
    @Expose
    private String publisherName;

    @SerializedName("writerName")
    @Expose
    private String writerName;
    @SerializedName("publisherId")
    @Expose


    private Integer publisherId;
    @SerializedName("writerId")
    @Expose
    private Integer writerId;

    public RoomBook() {
    }

    public RoomBook(String title1, String s, int i) {
        this.nativeName = title1;
        this.description = s;
        this.jumadaAlThani = i;
    }


    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getWriterName() {
        return writerName;
    }

    public void setWriterName(String writerName) {
        this.writerName = writerName;
    }

    public boolean isIs_update() {
        return is_update;
    }

    public void setIs_update(boolean is_update) {
        this.is_update = is_update;
    }

    public Integer getP_id() {
        return p_id;
    }

    public void setP_id(Integer p_id) {
        this.p_id = p_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Integer getBookJildNo() {
        return bookJildNo;
    }

    public void setBookJildNo(Integer bookJildNo) {
        this.bookJildNo = bookJildNo;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Integer isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Integer getIsRecorded() {
        return isRecorded;
    }

    public void setIsRecorded(Integer isRecorded) {
        this.isRecorded = isRecorded;
    }

    public Integer getIsUpcoming() {
        return isUpcoming;
    }

    public void setIsUpcoming(Integer isUpcoming) {
        this.isUpcoming = isUpcoming;
    }

    public Integer getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Integer isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getIsbnNumber() {
        return isbnNumber;
    }

    public void setIsbnNumber(String isbnNumber) {
        this.isbnNumber = isbnNumber;
    }

    public Long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public String getNormalName() {
        return normalName;
    }

    public void setNormalName(String normalName) {
        this.normalName = normalName;
    }

    public Long getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Long publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    public String getReadingType() {
        return readingType;
    }

    public void setReadingType(String readingType) {
        this.readingType = readingType;
    }

    public Integer getRecommendedBook() {
        return recommendedBook;
    }

    public void setRecommendedBook(Integer recommendedBook) {
        this.recommendedBook = recommendedBook;
    }

    public String getRomanName() {
        return romanName;
    }

    public void setRomanName(String romanName) {
        this.romanName = romanName;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getRealPages() {
        return realPages;
    }

    public void setRealPages(Integer realPages) {
        this.realPages = realPages;
    }

    public Integer getJumadaAlAwwal() {
        return jumadaAlAwwal;
    }

    public void setJumadaAlAwwal(Integer jumadaAlAwwal) {
        this.jumadaAlAwwal = jumadaAlAwwal;
    }

    public Integer getJumadaAlThani() {
        return jumadaAlThani;
    }

    public void setJumadaAlThani(Integer jumadaAlThani) {
        this.jumadaAlThani = jumadaAlThani;
    }

    public Integer getMuharram() {
        return muharram;
    }

    public void setMuharram(Integer muharram) {
        this.muharram = muharram;
    }

    public Integer getRabiAlAwwal() {
        return rabiAlAwwal;
    }

    public void setRabiAlAwwal(Integer rabiAlAwwal) {
        this.rabiAlAwwal = rabiAlAwwal;
    }

    public Integer getRabiAlThani() {
        return rabiAlThani;
    }

    public void setRabiAlThani(Integer rabiAlThani) {
        this.rabiAlThani = rabiAlThani;
    }

    public Integer getRajab() {
        return rajab;
    }

    public void setRajab(Integer rajab) {
        this.rajab = rajab;
    }

    public Integer getRamadan() {
        return ramadan;
    }

    public void setRamadan(Integer ramadan) {
        this.ramadan = ramadan;
    }

    public Integer getSafar() {
        return safar;
    }

    public void setSafar(Integer safar) {
        this.safar = safar;
    }

    public Integer getShaban() {
        return shaban;
    }

    public void setShaban(Integer shaban) {
        this.shaban = shaban;
    }

    public Integer getShawwal() {
        return shawwal;
    }

    public void setShawwal(Integer shawwal) {
        this.shawwal = shawwal;
    }

    public Integer getZulHijjah() {
        return zulHijjah;
    }

    public void setZulHijjah(Integer zulHijjah) {
        this.zulHijjah = zulHijjah;
    }

    public Integer getZulQadah() {
        return zulQadah;
    }

    public void setZulQadah(Integer zulQadah) {
        this.zulQadah = zulQadah;
    }

    public Long getReadBook() {
        return readBook;
    }

    public void setReadBook(Long readBook) {
        this.readBook = readBook;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public Integer getAudioId() {
        return audioId;
    }

    public void setAudioId(Integer audioId) {
        this.audioId = audioId;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

//        public List<Integer> getCatagories() {
//            return catagories;
//        }
//
//        public void setCatagories(List<Integer> catagories) {
//            this.catagories = catagories;
//        }
//
//        public Object getMediaResponse() {
//            return mediaResponse;
//        }
//
//        public void setMediaResponse(Object mediaResponse) {
//            this.mediaResponse = mediaResponse;
//        }
//
//        public Object getAudioResponse() {
//            return audioResponse;
//        }
//
//        public void setAudioResponse(Object audioResponse) {
//            this.audioResponse = audioResponse;
//        }
//
//        public Object getImageResponse() {
//            return imageResponse;
//        }
//
//        public void setImageResponse(Object imageResponse) {
//            this.imageResponse = imageResponse;
//        }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    public Integer getWriterId() {
        return writerId;
    }

    public void setWriterId(Integer writerId) {
        this.writerId = writerId;
    }


}


