package com.dawateislami.mylibrary.Room;

import java.lang.annotation.Native;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


@Dao
public interface NoteDaoClass {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insert(List<RoomBook> note);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    Long insert(RoomBook roomBook);

    @Update
    void update(RoomBook note);

//    @Query("Select * from book_table")
//    void  LiveData<List<RoomBook>> getall();

//




    ///LIST AUTOMATICALLY UPDATE OBSEVER
    @Query("Select * from book_table where language == :lang")
    LiveData<List<RoomBook>> getall(String lang);

    ///LIST AUTOMATICALLY UPDATE OBSEVER
//    @Query("Select * from book_table where isEnabled = 1  order by modifiedDate DESC")
//    LiveData<List<RoomBook>> getall();
//

    @Query("SELECT tt.*\n" +
            "FROM book_table tt\n" +
            "INNER JOIN\n" +
            "    (SELECT ID, MAX(modifiedDate) AS MaxDateTime\n" +
            "    FROM book_table WHERE isEnabled = 1\n" +
            "    GROUP BY book_table.readBook) groupedtt \n" +
            "ON tt.ID = groupedtt.ID \n" +
            "AND tt.modifiedDate = groupedtt.MaxDateTime order by tt.modifiedDate DESC ")
    LiveData<List<RoomBook>> getall();


  //  List<RoomBook> FinRoomBooks();

    @Query("select modifiedDate from book_table where isEnabled = 1 order by modifiedDate DESC LIMIT 1")
    Long getModifiedDate();

    @Query("select * from book_table where isEnabled = 1 and bookId = :bookid and language = :lang ")
    RoomBook getBook(Integer bookid ,String lang);

}
