package com.dawateislami.mylibrary.Room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {RoomBook.class},version =  1)
public abstract class NoteDatabase extends RoomDatabase {

    private static NoteDatabase instance;
    public abstract  NoteDaoClass noteDao();

    public static synchronized NoteDatabase getInstance(Context context) {
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),NoteDatabase.class,"book_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomcall)
                    .build();
        }
        return instance;
    }

    public static RoomDatabase.Callback roomcall = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
           // new populateDbAsyncTask(instance).execute();
        }
    };

//    private static class  populateDbAsyncTask extends AsyncTask<Void,Void,Void> {
//       // private NoteDao notedao;
//        private populateDbAsyncTask(NoteDatabase db){
//            //notedao = db.noteDao();
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//
//
//           // notedao.insert(new RoomBook("Title1","Description 1",1));
//          //  notedao.insert(new RoomBook("Tile1","Desription 2",2));
//            return null;
//        }
//    }

}