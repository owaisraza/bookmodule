package com.dawateislami.mylibrary.ApiCall;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientClass {


    private static Retrofit retrofit = null;
    public static String BASEURL = "";
    public static ApiInterfaceClass getClient(String baseurl) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//        https://www.dawateislami.net/wsbookslibrary/
//        https://www.dawateislami.net/wsbookslibrary/wsbookslibrary/
        retrofit = new Retrofit.Builder()
                   .baseUrl(baseurl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();



        return retrofit.create(ApiInterfaceClass.class);
    }

}
