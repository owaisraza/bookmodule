package com.dawateislami.mylibrary.ApiCall;

import com.dawateislami.mylibrary.Room.Note;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterfaceClass  {
    @GET("allbooksbylang?")
    public Call<Note> getBooks(@Query("app_id")int id,
                               @Query("lang_code")String langcode,
                               @Query("sync_datatime")Long timestamp,
                               @Query("pn")int pno,
                               @Query("ps")int limit);
}
