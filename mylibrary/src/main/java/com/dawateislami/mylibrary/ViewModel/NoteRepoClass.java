package com.dawateislami.mylibrary.ViewModel;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.dawateislami.mylibrary.ApiCall.ApiClientClass;
import com.dawateislami.mylibrary.Room.RoomBook;
import com.dawateislami.mylibrary.Room.Note;
import com.dawateislami.mylibrary.Room.NoteDaoClass;
import com.dawateislami.mylibrary.Room.NoteDatabase;
import com.dawateislami.mylibrary.View.onUpdateList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoteRepoClass  {

    private NoteDaoClass noteDao;
    public LiveData<List<RoomBook>> allNotes;

    List<RoomBook> dataList ;
    public NoteRepoClass(Application application) {
        NoteDatabase database  = NoteDatabase.getInstance(application);
        noteDao = database.noteDao();


    }

//    public void upsert(List<RoomBook> note)
//    {
//        long id = insert(note);
//        if (id == -1) {
//            update(entity);
//        }
//    }
//    }


    public  void update(RoomBook book){
        new upsertNoteAsyncTask(noteDao).execute(book);
    }

    public  List<Long> insert(List<RoomBook> note){
        try {
            return new insertNoteAsyncTask(noteDao).execute(note).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public  Long insertBook(RoomBook note){
        try {
            return  new insertBookAsyncTask(noteDao).execute(note).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public LiveData<List<RoomBook>> allNotes(String lang) {
        return  noteDao.getall(lang);
    }
    public LiveData<List<RoomBook>> allNotes() {
        return  noteDao.getall();
    }
    public Long getModefiedDate() {
        try {
            return   new modefiedDateAsyncTask(noteDao).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public RoomBook getBook(RoomBook book) {
        RoomBook b = null;
        try {
            b =  new getBookNoteAsynctask(noteDao).execute(book).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return b;

    }

    private static class insertNoteAsyncTask extends AsyncTask<List<RoomBook>,Void,List<Long>>{
        private NoteDaoClass notedao;
        public insertNoteAsyncTask(NoteDaoClass notedao) {
            this.notedao = notedao;
        }

        @Override
        protected List<Long> doInBackground(List<RoomBook>... lists) {
            List<Long> l = notedao.insert(lists[0]);
            return l;
        }
    }

    private static class getBookNoteAsynctask extends AsyncTask<RoomBook,Void,RoomBook>{
        private NoteDaoClass notedao;
        public getBookNoteAsynctask(NoteDaoClass notedao) {
            this.notedao = notedao;
        }


        @Override
        protected  RoomBook doInBackground(RoomBook... list) {
            RoomBook book =  notedao.getBook(list[0].getBookId(),list[0].getLanguage());
            return book;
        }
    }

    private static class upsertNoteAsyncTask extends AsyncTask<RoomBook,Void,Void>{
        private NoteDaoClass notedao;
        public upsertNoteAsyncTask(NoteDaoClass notedao) {
            this.notedao = notedao;
        }

        @Override
        protected Void doInBackground(RoomBook... lists) {
            notedao.update(lists[0]);
            return null;
        }
    }


    private static class modefiedDateAsyncTask extends AsyncTask<Void, Void, Long> {
        private NoteDaoClass notedao;
        public modefiedDateAsyncTask(NoteDaoClass notedao) {
            this.notedao = notedao;
        }

        @Override
        protected Long doInBackground(Void... voids) {
             Long l =  notedao.getModifiedDate();

                 if (l != null)
                     return l;
                 else
                     return 0l;

        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);

        }
    }

    private static class insertBookAsyncTask extends AsyncTask<RoomBook,Void,Long>{
        private NoteDaoClass notedao;
        public insertBookAsyncTask(NoteDaoClass notedao) {
            this.notedao = notedao;
        }

        @Override
        protected Long doInBackground(RoomBook... lists) {
            long l =  notedao.insert(lists[0]);
            return l;
        }
    }



    public List<RoomBook> populateAppGrid (String baseurl, int id, String langcode, Long timestamp, int pno, int limit, final onUpdateList mlist){

        dataList = new ArrayList<>();
//        ApiInterfaceClass i = ApiClientClass.getClient() ;
//        ApiClientClass.get

        Call<Note> call =  ApiClientClass.getClient(baseurl).getBooks(id,langcode,timestamp,pno,limit);
        call.enqueue(new Callback<Note>() {
            @Override
            public void onResponse(Call<Note> call, Response<Note> response) {

               // Note note = response.body();
                dataList = response.body().getRoomBooks();
                if(mlist!=null) {
                    mlist.updatedList(dataList);
                }
                if(dataList.size()!=0) {
                    insert(dataList);       ////passing whole list to insert in db
                }



//                for(int i = 0 ;i <dataList.size();i++) {
//
//
//                    Long l = insertBook(dataList.get(i));
//
//                    if(l == -1){
//
//                        dataList.get(i).setIs_update(true);
//                        update(dataList.get(i));
//                    }
//                }
//                Long l = getModefiedDate();
                Log.d("HSN","after insert "+dataList.size());
            }

            @Override
            public void onFailure(Call<Note> call, Throwable t) {
                Log.d("HSN",t.getMessage());

            }
        });

        return dataList;
    }
}
