package com.dawateislami.mylibrary.ViewModel;

import android.app.Application;

import com.dawateislami.mylibrary.Room.RoomBook;
import com.dawateislami.mylibrary.View.onUpdateList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class ViewModelClass extends AndroidViewModel {

    private NoteRepoClass repository;
    private LiveData<List<RoomBook>> allnote;

    public ViewModelClass(@NonNull Application application) {
        super(application);
        repository = new NoteRepoClass(application);

    }


    public void insert(List<RoomBook> roomBook){
        repository.insert(roomBook);
    }

    public void insertBook(RoomBook roomBook){
        repository.insertBook(roomBook);
    }


    public  void getBookFromServer(String baseurl, int id, String langcode, Long timestamp, int pno, int limit, onUpdateList mlist){


        repository.populateAppGrid(baseurl,id,langcode, timestamp, pno, limit,mlist);

    }
    public RoomBook getBookby_bookid(RoomBook book){
        return  repository.getBook(book);
    }

    public   LiveData<List<RoomBook>> getBook(String lang){
        return  repository.allNotes(lang);
    }
    public   LiveData<List<RoomBook>> getBook(){
        return  repository.allNotes();
    }
    public Long getModefiedDate() {
        return  repository.getModefiedDate();
    }
}
