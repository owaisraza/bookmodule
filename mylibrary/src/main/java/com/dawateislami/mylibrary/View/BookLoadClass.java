package com.dawateislami.mylibrary.View;

import android.content.Context;
import android.util.Log;

import com.dawateislami.mylibrary.Room.RoomBook;
import com.dawateislami.mylibrary.ViewModel.ViewModelClass;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class BookLoadClass extends AppCompatActivity {

    public static OnListReceive click;
    public static ViewModelClass noteViewModel ;
    public static  onUpdateList list;
    public static void init(Context cl,String baseurl,int id,String langcode,Long timestamp,int pno,int limit,String book_lang,onUpdateList mlist)
    {
        list = mlist;

        List<RoomBook> b = new ArrayList<>();
        noteViewModel =  ViewModelProviders.of((FragmentActivity) cl).get(ViewModelClass.class);

            noteViewModel.getBook(book_lang).observe((FragmentActivity) cl, new Observer<List<RoomBook>>() {
                @Override
                public void onChanged(List<RoomBook> roomBooks) {
                                Log.d("HSN", roomBooks.size()+"");

                    click.onReceiveList(roomBooks);
                }
            });


        hitCall(noteViewModel,baseurl,id,langcode,timestamp,pno,limit,list);
    }

    public static void init(Context cl,String baseurl,int id,String langcode,int pno,int limit,String book_lang,onUpdateList mlist)
    {
        list = mlist;

        List<RoomBook> b = new ArrayList<>();
        noteViewModel =  ViewModelProviders.of((FragmentActivity) cl).get(ViewModelClass.class);

        noteViewModel.getBook(book_lang).observe((FragmentActivity) cl, new Observer<List<RoomBook>>() {
            @Override
            public void onChanged(List<RoomBook> roomBooks) {
                Log.d("HSN", roomBooks.size()+"");

                click.onReceiveList(roomBooks);
            }
        });


        hitCall(noteViewModel,baseurl,id,langcode,getModefiedDate(),pno,limit,list);
    }

    public static void init(Context cl,String baseurl,int id,String langcode,int pno,int limit,onUpdateList mlist)
    {
        list = mlist;
        List<RoomBook> b = new ArrayList<>();
        noteViewModel =  ViewModelProviders.of((FragmentActivity) cl).get(ViewModelClass.class);

        noteViewModel.getBook().observe((FragmentActivity) cl, new Observer<List<RoomBook>>() {
            @Override
            public void onChanged(List<RoomBook> roomBooks) {
                Log.d("HSN", roomBooks.size()+"");

                click.onReceiveList(roomBooks);
            }
        });


        hitCall(noteViewModel,baseurl,id,langcode,getModefiedDate(),pno,limit,list);
    }
    public static void init(Context cl,String baseurl,int id,String langcode,int pno,int limit)
    {
        list = null;
        List<RoomBook> b = new ArrayList<>();
        noteViewModel =  ViewModelProviders.of((FragmentActivity) cl).get(ViewModelClass.class);

        noteViewModel.getBook().observe((FragmentActivity) cl, new Observer<List<RoomBook>>() {
            @Override
            public void onChanged(List<RoomBook> roomBooks) {
                Log.d("HSN", roomBooks.size()+"");

                click.onReceiveList(roomBooks);
            }
        });


        hitCall(noteViewModel,baseurl,id,langcode,getModefiedDate(),pno,limit,list);
    }
    public static void init(Context cl,String baseurl,int id,int pno,int limit,onUpdateList mlist)
    {
        list = mlist;
        List<RoomBook> b = new ArrayList<>();
        noteViewModel =  ViewModelProviders.of((FragmentActivity) cl).get(ViewModelClass.class);

        noteViewModel.getBook().observe((FragmentActivity) cl, new Observer<List<RoomBook>>() {
            @Override
            public void onChanged(List<RoomBook> roomBooks) {
                Log.d("HSN", roomBooks.size()+"");

                click.onReceiveList(roomBooks);
            }
        });


        hitCall(noteViewModel,baseurl,id,"",getModefiedDate(),pno,limit,list);
    }


    public static void setDataRecieveListener(OnListReceive mclick){
        click = mclick;
    }

    public static void hitCall(ViewModelClass noteViewModel,String baseurl,int id,String langcode,Long timestamp,int pno,int limit,onUpdateList list)
    {
        noteViewModel.getBookFromServer(baseurl,id,langcode, timestamp, pno, limit,list);
    }

    public static Long getModefiedDate() {
        return noteViewModel.getModefiedDate();
    }


    public  static  RoomBook getBookby_Bookid(RoomBook roomBook)
    {
        RoomBook book =   noteViewModel.getBookby_bookid(roomBook);
        return book;
    }
}
