package com.dawateislami.mylibrary.View;

import com.dawateislami.mylibrary.Room.RoomBook;

import java.util.List;

public  interface OnListReceive {

    public void onReceiveList(List<RoomBook> booklist);
}
